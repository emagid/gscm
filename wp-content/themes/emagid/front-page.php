<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
                <p><?php the_field('hero_subtitle'); ?></p>
                <a href='/contact' class='button'> CONTACT </a>
            </div>
        </div>
	</section>
    <section class="small_hero" style="background-image:url(<?php the_field('banner_small'); ?>)">
        <div class="overlay">
            <div class='text_box'>
<!--                <p>WE INVEST IN RELATIONSHIPS</p>-->
            </div>
        </div>
    </section>

	<!-- HERO SECTION END -->



	<!-- LOGO SECTION -->
	<section class='client_logos'>
		<h2><?php the_field('portfolio_section_header'); ?></h2>
		<div>
<!-- 			<div class='logos'>
				<a href="portfolioPage.html">
					<div class='overlay'><p>VIEW NOW</p></div>
					<img class="client_logo grey" src="<?php echo get_template_directory_uri(); ?>/assets/img/certify_color.png">
				</a>
			</div> -->
			<p><?php the_field('portfolio_section_text'); ?></p>
		</div>
		<a class='view_more' href="/investments"><button class='button lets_talk'>VIEW ALL COMPANIES</button></a>
	</section>
	<!-- LOGO SECTION END -->



	<!-- ABOUT US SECTION -->
	<div class="slider-content">
<!--
		<section class='about_us'>
			<div class='float_right_img' style="  background-image: url(<?php the_field('slider_image'); ?>);"></div>
			<div class='about_text'>
				<h2><?php the_field('slider_header'); ?></h2>
				<div>
					<p><?php the_field('slider_text'); ?></p>
				</div>
			</div>
		</section>
		<section class='about_us'>
			<div class='float_right_img' style="  background-image: url(<?php the_field('slider_image_2'); ?>);"></div>
			<div class='about_text'>
				<h2><?php the_field('slider_header_2'); ?></h2>
				<div>
					<p><?php the_field('slider_text_2'); ?></p>
				</div>
			</div>
		</section>
-->
		<section class='about_us'>
			<div class='float_right_img' style="  background-image: url(<?php the_field('slider_image_3'); ?>);"></div>
			<div class='about_text'>
				<h2><?php the_field('slider_header_3'); ?></h2>
				<div>
					<p><?php the_field('slider_text_3'); ?></p>
				</div>
			</div>
		</section>
	</div>


	
	<!-- ABOUT US END  -->


	<!-- GROWTH -->
	<section class='growth'>
		<div>
			<h2><?php the_field('growth_section_header'); ?></h2>
			<p><?php the_field('growth_section_text'); ?></p>
            
            <div class="talk_button">
                <div>
                    <a href="/contact">
                       <button class='lets_talk button'> LETS TALK </button>
                    </a>
                </div>
            </div>
            
		</div>

		<div>
			<img class='partofteam' src="<?php echo get_template_directory_uri(); ?>/assets/img/partofteam.png" width="90%" alt="BECOME PART OF THE TEAM">
<!--			<h4>STEP ONE</h4>-->
			<p><?php the_field('step_one'); ?></p>
		</div>

    </section>
	<!-- GROWTH -->

  <script type="text/javascript">
    $(document).ready(function(){
      $('.slider-content').slick({
        autoplay:true,
		autoplaySpeed:2500,
		dots: false
      });
    });
  </script>

<?php
get_footer();
