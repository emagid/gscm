<?php
/**
 * The template for displaying all pages
 * 
 * Template Name: Contact Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="height: 250px; background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box' id="contact_hero_text" style="margin-bottom: 0px;">
                <h1>WORK WITH US</h1>
            </div>
        </div>
    </section>
    
	<!-- HERO SECTION END -->

	<!-- <section class='bio'>
        <div class="form">
            <h2><?php the_field('title'); ?></h2>
            <div class="form_left">
                
                <?php the_field('content'); ?>
            </div>
            <div class="form_right">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3097.853411359685!2d-77.1621939846447!3d39.06425467954526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7cd02accbc52f%3A0x5f2f4bf9f2703b46!2s1201+Seven+Locks+Rd+%23300%2C+Rockville%2C+MD+20854!5e0!3m2!1sen!2sus!4v1517958673177" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div>
        
        </div>
	</section> -->

    <section class='bio'>
        <p class="auto_center" style="text-align: center; font-weight: 400;"><?php the_field('title'); ?></p>
        <?php the_field('content'); ?>
	</section>


<?php
get_footer();
