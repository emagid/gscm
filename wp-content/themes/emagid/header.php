<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="google-site-verification" content="uEafgaQcnVcKjlgclwGPKKmIEGHR1RS9_BNwak0k_3s" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
<!--    CUSTOM STYLE-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/urock.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/langdon/font.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    
    
<!--    CUSTOM SCRIPT-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116457979-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116457979-1');
</script>
    
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- NAVIGATION BAR MENU -->
	<header>
		<a href="/" class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gscm.png" alt="Granite State Capital Management">
            </a>
		<nav>
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
		</nav>

		<!-- Mobile -->
		<div id="hamburger">
            <div class="bar1 hamburger_line"></div>
            <div class="bar2 hamburger_line"></div>
            <div class="bar3 hamburger_line"></div>
        </div>

        <div class='dropdown'>
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
        </div>
        <!-- Mobile Ends -->
	</header>
	<!-- NAVBAR ENDS -->


