$(document).ready(function(){

	// Circle load
	$('.circle_wrapper').addClass('moveUp')


	$("#hamburger").on('click', function(e){
	    $(".dropdown").slideToggle(100);
	    $(".dropdown").toggleClass('flex');
	    $(".bar1").toggleClass('change1');
	    $(".bar2").toggleClass('change2');
	    $(".bar3").toggleClass('change3');

	    $('body').toggleClass('noScroll');
	});


	// client logo hover
	$('.logos a').mouseover(function(){
		$($(this).children('img')).removeClass('grey')
	});

	$('.logos a').mouseleave(function(){
		$($(this).children('img')).addClass('grey')
	});

	// Team hover
	$('.team').mouseover(function(){
		$($(this).children('.button')).fadeIn();
	});


	$('.team').mouseleave(function(){
		$($(this).children('.button')).fadeOut();
	});

	$('.team img').mouseover(function(){
		$(this).removeClass('grey');
	});


	$('.team img').mouseleave(function(){
		$(this).addClass('grey');
	});


	// logohover
	$('.portfolio a').mouseover(function(){
		$($(this).children('img')).removeClass('grey')
	});

	$('.portfolio a').mouseleave(function(){
		$($(this).children('img')).addClass('grey')
	});


});
